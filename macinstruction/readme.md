# Installation of apps on Mac

## Install Homebrew
- https://brew.sh/ - To learn about homebrew
- Download homebrew if its not installed already
```
brew --version
```

- if above does not exist

```
pwd
ls -l
cd Downloads  # make sure directory exists

# Download brew installer

curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh > set.sh

# make set.sh executable

chmod + x set.sh
```

- Run set.sh
```
./set.sh
```
- After running above, add brew to path by following instructions from the above output, usually looks like commands below

```
(echo; echo 'eval "$(/opt/homebrew/bin/brew shellenv)"') >> /Users/ck/.zprofile

eval "$(/opt/homebrew/bin/brew shellenv)"

brew --version
```


## Install Python
- Check python and install python if not found via terminal
- Install python > 3.10
```
python --version

# if above not installed

brew install python

# check python

python3 --version
pip3 --version
```

## Install Git
- Install git from terminal
```
git --version

# if above not installed

brew install git
```


## Install Docker
- Follow instructions from 
https://docs.docker.com/desktop/install/mac-install/
- Download Docker based on chipset Intel or Apple silicon
- Follow instruction to install using terminal
```
sudo hdiutil attach Docker.dmg
sudo /Volumes/Docker/Docker.app/Contents/MacOS/install
sudo hdiutil detach /Volumes/Docker

```

## Minikube
- Install minikue from terminal
```
brew install minikube
```
- Once installed, re open terminal
```
minikube start
kubectl get svc
minikube stop
```

## Install Ansible
```
brew install ansible
ansible --version
```

## Jenkins
- Download jenkins docker image
```
docker pull jenkins/jenkins:lts-jdk17
```

## Vs-Code
- Install Vscode from terminal
```
brew install --cask visual-studio-code
```
- Open vscode
  - Goto extension
      - Add Remote-SSH from Microsoft
      - Python from Microsoft
      - YAML from Red Hat
      - autopep8 from Microsoft
      - Docker from Microsoft